/**
 * @author Rivest, 2015
 *
 */

public class Demo {

	final static String ServerIP1 = "137.94.172.117";
	final static String ServerIP2 = "137.94.172.97";
	
	final static int myAIID = 301;
	final static int myTable = 3;
	final static int myPW = 309;
	final static int size = 8; //Play on a 8x8
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		GameSocket gs = new GameSocket();
		
		//Connect
		int res = gs.connect(ServerIP1, myAIID, myTable, myPW, GameSocket.ANY, size);
		
		//Read result, if fail, display error message
		if (res < 0) {
			System.out.println(gs.connMsg);
			return;
		}
		
		//first message should be PLEASE_PLAY since bot is second
		int[] msg = gs.readMessage(); 
		if (msg[0] != GameSocket.PLEASE_PLAY) {
			throw new Exception("NOT MY TURN?");
		}
		
		//While the game is active
		while (!((msg[0] == GameSocket.CLOSING) || (msg[0] == GameSocket.GAME_OVER))) {
			
			do {
				//play randomly
				int row, col;
				row = (int) (Math.random()*(size));
				col = (int) (Math.random()*(size));
				//Send move
				gs.sendMove(row, col);
				//Read result
				msg = gs.readMessage();
				//Read next message
				msg = gs.readMessage();
			} while (msg[0] == GameSocket.PLEASE_PLAY);
				
			//Then wait for my turn
			while (!((msg[0] == GameSocket.PLEASE_PLAY) || (msg[0] == GameSocket.CLOSING) || (msg[0] == GameSocket.GAME_OVER))) {
				msg = gs.readMessage();
			}
			
		}
		
		
		

	}

}
